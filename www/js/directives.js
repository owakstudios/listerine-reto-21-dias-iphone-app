/* global Snap */
'use strict';

/**
 * Custom application directives
 * Created by Andrés Osorio<AndresOsorio@owak.co> on 2/24/15.
 */

var AppDirectives = angular.module('ListerineReto21Dias.directives', []),
directivesUrl     = 'templates/directives/listerine';

/**
 * ListerineNavBar
 *
 * custom directive that renders the application nav bar.
 * We do not use ionic's default navigation bar because the navbar requires
 * custom markup and style.
 */
AppDirectives.directive('listerineNavbar', function ($previousState) {

    var controller = function($scope) {

        $scope.previousState = function() {
            $previousState.go();
        }
    };
    return {
        restrict    : 'EA',
        replace     : true,
        scope       : { nextState : '@', hasNextButton : '@', hasBackButton: '@' },
        templateUrl : directivesUrl + '/navbar.html',
        controller  : controller
    }
})

/**
 * QuestionCard
 *
 * custom directive for rendering a question card.
 * It also handles all necessary DOM and animation events
 * for every card.
 */
AppDirectives.directive('questionCard', function () {

    /*
     * directive controller
     *
     * determines the actions behaviour according to the properties
     * of the specified question (model)
     */
    var controller = function($scope) {
        $scope.hasSubquestions = typeof($scope.question.sub_questions) !== 'undefined';
        $scope.registerAnswer = !$scope.hasSubquestions;
        $scope.answer = null;

        /**
         * saves the answer for this question card (only if the question is enabled to be answered)
         * @param answer
         */
        $scope.answerQuestion = function(answer) {
            if( $scope.hasSubquestions && answer === 'YES') {
                $scope.registerAnswer = true;
            }
            if ( $scope.registerAnswer ) {
                $scope.onAnswer({ question : $scope.question, answer : answer });
                $scope.answer = answer;
            }
        }
    };

    /**
     * Directive Link function
     *
     * bind special events to the DOM elements of this directive.
     * Handles the DOM login for questions that have subquestions.
     *
     * @param scope
     * @param element
     * @param attrs
     */
    var link = function ( scope, element, attrs ) {
        var hasSubquestions = typeof(scope.question.sub_questions) !== 'undefined';

        scope.registerAnswer = !hasSubquestions;
        scope.answer = null;

        element.find('#answer-no').bind('click', function () {
            if ( scope.hasSubquestions && !scope.registerAnswer ) {
                var subquestion = scope.question.sub_questions[ 0 ].question;
                scope.registerAnswer = true;
                element.find('h2').html(subquestion);
            }
        });
    };

    return {
        restrict    : 'E',
        replace     : true,
        scope       : { question : '=', onAnswer : '&' },
        templateUrl : directivesUrl + '/question-card.html',
        link        : link,
        controller : controller
    }
});

/**
 * OralCareScore
 *
 * custom directive for controlling the oral care score meter through SVG
 */
AppDirectives.directive('oralCareScore', function () {

    /**
     * possible values the score can return
     * @type {number[]}
     */
    var possibleValues = [ 0, 33, 67, 100 ];

    /**
     * controller function. for now, it watches the score value and
     * animates the SVG accordingly
     * @param $scope
     * @param $timeout
     */
    var controller = function ( $scope, $timeout ) {
        //methods and animation triggers
        $scope.$watch('score', function ( newValue, oldValue ) {
            //start animation (with delays)
            animateScoreMilestonesUntil(newValue, $scope.sliderEl);
            //-----------------------------------------------

            //animate score bar
            animateScoreBarUntil(newValue, $scope.sliderEl, $scope.sliderScoreBar);
        });
    };

    /**
     * directive link function.
     * it initializes the svg with transformations so it can be animated later and
     * saves SVG elements to the scope for later use
     * @param scope
     * @param element
     * @param attrs
     */
    var link = function ( scope, element, attrs ) {
        var scoreSlider = Snap('#oral-care-score');

        var scoreScales = possibleValues.map(function ( num ) {
            return scoreSlider.select('#oral-care-score-' + num + 'p');
        });

        var scoreBar = scoreSlider.select('#score-slider');

        //prepare slider for presentation by setting elements in initial state
        reset(scoreSlider, scoreScales, scoreBar);

        scope.sliderEl = scoreSlider;
        scope.sliderScoreCircles = scoreScales;
        scope.sliderScoreBar = scoreBar;
    };

    /**
     * set SVG properties of some elements so they can be animated in later
     * @param slider
     * @param scoreCircles
     * @param bar
     */
    var reset = function ( slider, scoreCircles, bar ) {
        scoreCircles.forEach(function ( circle ) {
            circle.attr({ transform : 's0 0' });
        });

        bar.attr({ width : 0 });
    };

    /**
     * animates slider milestones until the specified number (score)
     * @param until
     */
    var animateScoreMilestonesUntil = function ( until, sliderEl ) {
        //start animation (with delays)
        possibleValues.forEach(function ( value, index ) {
            //stop at the 'until' number (so only the passed milestones are animated in)
            if ( value > until ) {
                return;
            }

            var milestoneId = '#oral-care-score-' + value + 'p',
                milestone = sliderEl.select(milestoneId),
                delay = 700 * (index + 1);

            //animate the current milestone, adding the specified delay
            setTimeout(function () {
                milestone.animate({
                    transform : 's1 1'
                }, 1000, mina.elastic);
            }, delay);
        });
    };

    /**
     * animate the score bar until the specified milestone
     * @param until
     */
    var animateScoreBarUntil = function ( until, sliderEl, scoreBar ) {
        var milestoneId = '#oral-care-score-' + until + 'p',
            milestone = sliderEl.select(milestoneId),
            targetW = milestone.attr('cx') - milestone.attr('r');

        scoreBar.animate({
            width : targetW
        }, 2400, mina.easein());
    };

    //--------------------------------------------------------------

    return {
        restrict    : 'EA',
        replace     : true,
        scope       : { score : '=' },
        templateUrl : directivesUrl + '/oral-care-score.svg',
        controller  : controller,
        link        : link
    };
});

/*
 * custom directive that renders a single button of oral care,
 * and handles all its states and actions
 */
AppDirectives.directive('listerineOralCareButton', function () {

    /**
     * Directive Link function
     *
     * handles activation state for the button
     *
     * @param scope
     * @param element
     * @param attrs
     */
    var link = function ( scope, element, attrs ) {

        scope.step_image_url = scope.step;

        element.find('a').on('click', function () {
            scope.onActivate({ step : scope.step });
            scope.isActive = true;
        });

        scope.$watch(function () {
            return element.attr('class');
        }, function ( value ) {
            scope.isActive = element.hasClass('active');
        });
    };

    var controller = function($scope) {
        var activeImage = $scope.step + '-active';

        $scope.$watch('isActive', function() {
            $scope.step_image_url = $scope.isActive ? activeImage : $scope.step;
        });
    };

    return {
        restrict    : 'E',
        scope       : {
            step        : '@',
            onActivate  : '&',
            targetState : '@'
        },
        transclude  : true,
        replace     : true,
        templateUrl : directivesUrl + '/oral-care-button.html',
        link        : link,
        controller : controller
    }
});

/*
 * custom directive for the 3 buttons with the 3 steps of oral health care
 */
AppDirectives.directive('listerineOralCareMenu', function () {

    return {
        restrict    : 'E',
        transclude  : true,
        replace     : true,
        templateUrl : directivesUrl + '/oral-care-menu.html'
    }
});