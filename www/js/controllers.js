'use strict';

/**
 * Application controllers
 * Created by Andrés Osorio<AndresOsorio@owak.co> on 2/24/15.
 */
var AppControllers = angular.module('ListerineReto21Dias.controllers', []);

/**
 * Application controller for general handling of sessions and navigation
 */
AppControllers.controller('ApplicationController', function ( $scope, $state, $previousState, $ionicScrollDelegate, Session ) {

    $scope.sessionExists = false;

    //------------------------------------------------
    $scope.$watch(function () {
        return Session.current();
    }, function () {
        $scope.sessionExists = Session.exists();
    });

    $scope.$watch(function () {
        return Session.current();
    }, function () {
        $scope.user = Session.current() ? Session.current().user : null;
    });
    //------------------------------------------------

    $scope.scrollTop = function () {
        $ionicScrollDelegate.scrollTop();
    };

    $scope.previousState = function () {
        $previousState.go();
    }

    $scope.logout = function () {
        $scope.loginPromise = Session.destroy();

        if($state.current.name !== 'intro') {
            $scope.loginPromise.then(function () {
                $state.go('intro');
            }, function () {
                $state.go('intro');
            });
        }
    };

});

/**
 * Login Controller
 *
 * Handles all events and views for the login screen. Uses the Session services
 * for actually logging in.
 */
AppControllers.controller('LoginController', function ( $scope, $state, Session ) {
    //saves email and password
    $scope.user = {};
    $scope.errors = null;

    /**
     * logs the user in using the Session Service
     */
    $scope.login = function () {
        var loginRequest = Session.create($scope.user.email, $scope.user.password);

        loginRequest.success(function (response) {
                if(response === "") {
                    $scope.errors = "hubo un error inesperado, por favor, intentelo de nuevo";
                    clearFormData();
                } else {
                    $state.go('assessment.questions');
                }
            })
            .error(function ( errors ) {
                $scope.errors = errors;
                clearFormData();
            });
        //make the promise accessible for angular ngBusy
        $scope.loginPromise = loginRequest;
    };

    $scope.logout = function () {
        $scope.loginPromise = Session.destroy();
    };

    var clearFormData = function () {
        $scope.user.email = '';
        $scope.user.password = '';
    }
});

/**
 * Registration Controllers
 *
 * Handles all interface events for the user (professional) registration process
 */
AppControllers.controller('RegisterController', function ( $scope, $state, $filter, $ionicPopup, $ionicScrollDelegate, Session, Server ) {
    // save the new user we will be registering
    $scope.newUser = {};

    //available gender options
    $scope.genderOptions = [
        { id : 'M', name : 'Masculino' },
        { id : 'F', name : 'Femenino' }
    ];

    /**
     * controller action for registering new professionals.
     * Uses the Server Service
     */
    $scope.register = function () {
        //copy the ngModel since we need to format it for the server
        var info = angular.copy($scope.newUser, {});
        info.dob = $filter('date')($scope.newUser.dob, 'yyyy/MM/dd');
        info.gender = $scope.newUser.gender.id;

        //call registration services here
        var newAccountRequest = Server.createAccount(info);

        newAccountRequest.then(function ( response ) {
            //save the new user session and show a confirmation popup
            Session.set(response.data);
            confirmRegistration();
        }, function ( errors ) {
            //if there were any errors, show them,
            //clear all fields that had invalid data and scroll to the top
            $scope.errors = errors.data;
            clearInvalidFields(errors.data);
            $ionicScrollDelegate.scrollTop(true);
            notifyValidationErrors();
        });

        $scope.registerPromise = newAccountRequest;
    };

    /**
     * show an alert confirming that the user account was created
     */
    var confirmRegistration = function () {
        var alertPopup = $ionicPopup.alert({
            title    : '¡Gracias por inscribirse!',
            template : 'Su cuenta ha sido creada exitosamente.'
        });
        alertPopup.then(function ( res ) {
            $state.go('assessment.questions');
        });
    };

    var notifyValidationErrors = function () {
        var alertPopup = $ionicPopup.alert({
            title    : '¡Error en la inscripción!',
            template : 'No hemos podido registrarlo en nuestra comunidad debido a un error. Por favor, revise la información insertada e inténtelo de nuevo'
        });
    }

    /**
     * clears all the fields that had invalid data
     * @param invalidFields
     */
    var clearInvalidFields = function ( invalidFields ) {
        for ( var field in invalidFields ) {
            $scope.newUser[ field ] = null;
        }
    };
});

/**
 * Assessment Controller
 *
 * Manages all states and event of the assessment process of this app.
 * This controller saves the assessment's answers and displays the
 * proper state according to the assessment results.
 */
AppControllers.controller('AssessmentController', function ( $scope, $state, $previousState, $ionicHistory, Question, Assessment, $ionicScrollDelegate ) {
    $scope.score = 0;       //saves the score of the assessment

    $scope.init = function () {
        $scope.answers = {};      //save assessment answers
        $scope.completed = 0;       //observe completion of the assessment

        //retrieve all assessment questions from a local JSON file
        Question.all().success(function ( questions ) {
            $scope.questions = questions;
        });
    }

    /**
     * saves the answer of a question in the scope, so it can be used later when
     * calculating the assessment score.
     *
     * @param question
     * @param answer
     */
    $scope.respondQuestion = function ( question, answer ) {
        console.log('responding question:' + question.id + ' | Answer: ' + answer);

        //if question is being answered for the first time, update assessment completion
        if ( typeof($scope.answers[ question.id ]) === 'undefined' ) {
            $scope.completed += 1;
        }
        //save the answer
        $scope.answers[ question.id ] = answer;

        //trigger assessment calculation when all questions are answered
        if ( $scope.completed === 3 ) {
            //calculate score
            calculateScore();
        }
    };

    $scope.previousState = function () {
        $previousState.go();
    };

    /**
     * call the Assessment service to calculate the resulting score
     * of the assessment process
     */
    var calculateScore = function () {
        $scope.scorePromise = Assessment.calculate($scope.answers);
        $ionicScrollDelegate.scrollTop(true);   //scroll view to top

        //once the score has been calculated, save the result
        //and trigger the result substate of the assessment
        $scope.scorePromise.then(function ( score ) {
            $scope.score = score;
            $scope.init();
            $state.go('assessment.result');
        });
    };
});

//-------------------------------------------------------------------------------------------------------------

/**
 * OralCareStepsController
 *
 * Handles all the events of the oral care steps screen (where we show the "cleanliness" of the mouth after some oral
 * care steps).
 */
AppControllers.controller('OralCareStepsController', function ( $scope, $state ) {
    //autoload the current step substate (so the screen is never blank)
    $scope.activeStep = $state.current.name.split('.')[ 1 ];

    $scope.$watch(function () {
        return $state.current.name.split('.')[ 1 ];
    }, function () {
        $scope.activeStep = $state.current.name.split('.')[ 1 ];
    });

    /**
     * switches the currently active step (between brushing, flossing and rinsing)
     * @param step
     */
    $scope.changeActiveStep = function ( step ) {
        $scope.activeStep = step;
    };
});

/**
 * OralCareTipsController
 *
 * handles all events of the oral care tips tips screen (different tips for each oral care step).
 */
AppControllers.controller('OralCareTipsController', function ( $scope, $ionicSlideBoxDelegate, $state ) {
    //autoload the current step substate (so the screen is never blank)
    $scope.activeStep = $state.current.name.split('.')[ 1 ];
    $scope.isModalOpen = false;

    $scope.$watch(function () {
        return $state.current.name.split('.')[ 1 ];
    }, function () {
        $scope.activeStep = $state.current.name.split('.')[ 1 ];
    });

    /**
     * switches the currently active step (between brushing, flossing and rinsing)
     * @param step
     */
    $scope.changeActiveStep = function ( step ) {
        $scope.activeStep = step;
    };

    /*
     * custom modal for "mouthwash" tips events for opening and closing
     */
    $scope.openModal = function () {
        //open modal object
        $scope.isModalOpen = true;
    };

    $scope.closeModal = function () {
        //close modal object
        $scope.isModalOpen = false;
    };

    /*
     * custom slide buttons
     */

    $scope.$watch(function () {
        return $ionicSlideBoxDelegate.currentIndex();
    }, function () {
        var current = $ionicSlideBoxDelegate.currentIndex(),
            total = $ionicSlideBoxDelegate.slidesCount();

        $scope.showPrevious = (current !== 0);
        $scope.showNext = (current !== total - 1);
    }, true);

    $scope.slidePrevious = function () {
        $ionicSlideBoxDelegate.previous();
    };

    $scope.slideNext = function () {
        $ionicSlideBoxDelegate.next();
    };
});

//-------------------------------------------------------------------------------------------------------------

/**
 * Challenge Controller
 *
 * handles events and switching of states for the "explicacion reto 21 dias" screen
 */
AppControllers.controller('ChallengeController', function ( $scope, $state, Session ) {
    //autoload the current step substate (so the screen is never blank)
    $scope.activeDay = $state.current.name.split('.')[ 1 ];
    $scope.hasSession = Session.exists();

    $scope.$watch(function () {
        return $state.current.name.split('.')[ 1 ];
    }, function () {
        $scope.activeDay = $state.current.name.split('.')[ 1 ];
    });

    $scope.$watch(function () {
        return Session.exists();
    }, function () {
        $scope.hasSession = Session.exists();
    });

    /**
     * changes the active day (1, 7, 14, 21)
     * @param day
     */
    $scope.changeActiveDay = function ( day ) {
        $scope.activeDay = day;
    }
});

//-------------------------------------------------------------------------------------------------------------

/**
 * UserEngageController
 *
 * handles events and actions for the user engage screen, where the consumer submits his/her email
 * to receive information about the campaign.
 */
AppControllers.controller('UserEngageController', function ( $scope, $state, $ionicPopup, Server, Session ) {
    $scope.user = {};

    $scope.subscribeUser = function () {
        //retrieve the current professional session
        var session = Session.current();

        //send the email request to the server and wait for a response
        var request = Server.sendEmailToUser({
            name            : $scope.user.name,
            email           : $scope.user.email,
            professional_id : session.user.id
        });

        request.success(function ( response ) {
            popup({
                title    : 'Muchas Gracias!',
                template : response.message
            }).then(function ( res ) {
                $state.go('listerine');
            });
        })
            .error(function () {
                console.log(' failed!');
                popup({
                    title    : 'Error',
                    template : 'Lo sentimos, hubo un error inesperado al enviar el correo. Inténtelo más tarde'
                }).then(function ( res ) {
                    $state.go('challenge.day21');
                });
            });

        $scope.emailPromise = request;
    };

    var popup = function ( params ) {
        var alertPopup = $ionicPopup.alert(params);
        return alertPopup;
    };
});