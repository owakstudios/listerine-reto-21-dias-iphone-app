'use strict';

/**
 * Application Services
 *
 * These services are meant for decoupling application core functionality (business logic)
 * from the views and controllers
 * @type {*|module}
 *
 * Created by Andrés Osorio<AndresOsorio@owak.co> on 2/24/15.
 */
var AppServices = angular.module('ListerineReto21Dias.services', []);

/**
 * Server Url Constant
 *
 * points to the server that has the API endpoints that allow the app to work.
 * NOTE: Change this url when moving to production to https://listerine.co/reto21dias2015/public/index.php
 */
//AppServices.constant('SERVER_URL', '');
//AppServices.constant('SERVER_URL', 'http://listerine-reto21dias.local');
AppServices.constant('SERVER_URL', 'https://listerinesalud.com/reto21dias2015/public/index.php');

//--------------------------------------------------------------------------------------------------------------
/* ------------------------------------------- *
 * API and network services
 * ------------------------------------------- */

 /**
 * Session Service
 *
 * This service handles all logic related to the process of session handling.
 * It allows the application to perform operations as login, logout, handle
 * authentication tokens for API requests and keep track of the currently logged in user.
 */
AppServices.factory('Session', function ( $http, SERVER_URL ) {
    var apiAuthUrl = SERVER_URL + '/api/authenticate';
    var currentSession = null;

    /**
     * logs a user into the application's server api and saves the auth token
     * @param email
     * @param password
     * @returns {HttpPromise}
     */
    var create = function ( email, password ) {
        //communicate with the API
        var authRequest = $http.post(apiAuthUrl, { email : email, password : password });

        authRequest.success(function ( response ) {
            save(response);
        });

        return authRequest;
    };

    /**
     * saves the currently active session into the web session storage
     * @param session
     */
    var save = function ( session ) {
        sessionStorage.setItem('user_session', JSON.stringify(session));
        currentSession = session;
    };

    /**
     * logs the user out and destroys the current auth token
     * @returns {*}
     */
    var destroy = function () {
        //communicate with the API
        var logoutRequest = $http.delete(apiAuthUrl, { token : currentSession.token });

        logoutRequest.success(function () {
            currentSession = null;
            sessionStorage.clear();
        }).error(function() {
            currentSession = null;
            sessionStorage.clear();
        });

        return logoutRequest;
    };

    /**
     * retrieves the currently active session
     * @returns {*}
     */
    var current = function () {
        if ( !sessionStorage.getItem('user_session') ) {
            return null;
        }
        if ( currentSession === null ) {
            var sessionString = sessionStorage.getItem('user_session');
            if(sessionString) {
                currentSession = JSON.parse(sessionString);
            }
        }
        return currentSession;
    };

    /**
     * replaces the current session info (if any) with the one provided.
     * @param sessionInfo
     */
    var set = function ( sessionInfo ) {
        save(sessionInfo);
    };

    /**
     * returns true if there is an active session
     * @returns {boolean}
     */
    var exists = function() {
        return typeof(current()) !== 'undefined' && current() !== null;
    };

    return {
        create  : create,
        destroy : destroy,
        current : current,
        set     : set,
        exists : exists
    }
});

/**
 * Session HTTP Injector
 *
 * this injector extends the $http service of Angular
 * to properly prepare all API requests with the authentication token
 * of the currently logged in user (by setting a custom HTTP header)
 */
AppServices.factory('sessionInjector', function() {
    var sessionInjector = {
        request: function(config) {
            if (sessionStorage.getItem('user_session')) {
                var session = JSON.parse(sessionStorage.getItem('user_session'));
                config.headers['X-Auth-Token'] = session.token;
            }
            return config;
        }
    };
    return sessionInjector;
});

/**
 * Server Service
 *
 * this services abstracts the available functionality of the public ser API
 * to simple methods to be called through the entire application
 */
AppServices.factory('Server', function ( $http, Session, SERVER_URL ) {

    /**
     * tells the server to send the appropiate email to the engaged user
     * @param userInfo
     * @returns {HttpPromise}
     */
    var sendEmailToUser = function ( userInfo ) {
        return $http.post(SERVER_URL + '/api/consumers/engage', userInfo);
    };

    /**
     * send the new professional information to the server, in order to create a new professional account
     * @param professionalInfo
     * @returns {HttpPromise}
     */
    var createAccount = function ( professionalInfo ) {

        var registrationRequest = $http.post(SERVER_URL + '/api/professionals', professionalInfo);

        registrationRequest.success(function ( response ) {
            Session.set(response);
        });

        return registrationRequest;
    }

    return {
        sendEmailToUser : sendEmailToUser,
        createAccount   : createAccount
    }
});

//--------------------------------------------------------------------------------------------------------------
/* ------------------------------------------- *
 * Local Services
 * ------------------------------------------- */
/**
 * Question Resource
 *
 * represents a single question in the assessment process.
 *
 * Questions have a body (the actual question) and an array of any subquestions
 * that should be asked after responding the main one.
 */
AppServices.factory('Question', function ( $http ) {

    /**
     * retrieves all questions from database (local json file)
     * @returns {HttpPromise}
     */
    var all = function () {
        return $http.get('js/fixtures/questions.json');
    };

    return {
        all : all
    };
});

/**
 * Assessment Service
 *
 * This services performs the necessary (dark) procedures to calculate
 * the assessment's oral care score of the user based on assessment answers
 */
AppServices.factory('Assessment', function ($q) {
    var maxScore = 100;

    /**
     * triggers the calculation of the oral care score result
     * (uses a nice trick with promises, so that the assessment results,
     * appear to be calculating in a long time for user experience)
     * @param answers
     * @returns {*}
     */
    var calculate = function(answers) {
        return $q(function(resolve, reject) {
            setTimeout(function() {
                var result = performCalculation(answers);
                resolve(result);
            },3000);
        });
    };

    /**
     * performs the oral care score calculation
     * @param answers
     * @returns {number}
     */
    var performCalculation = function ( answers ) {
        var numQuestions = Object.keys(answers).length,
            score = maxScore;

        //count the YES occurrences
        for ( var questionId in answers ) {
            var answer = answers[ questionId ];
            if ( answer === 'YES' ) {
                //discount one third of the max score
                score -= (maxScore / numQuestions);
            }
        }

        return Math.round(score);
    };

    return {
        calculate : calculate
    }
});