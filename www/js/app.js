'use strict';

/**
 * ========================================================================================
 * LISTERINE® RETO 21 DIAS: iPhone Application for ORal Care Professionals
 * ========================================================================================
 * This is a custom hybrid iPhone application built for oral care professionals
 * who recommend the use of LISTERINE to their patients. The purpose is to
 * involve the HCP professionals into the campaign "Reto 21 dias" by becoming
 * "influencers" or "evangelists" that drive their patients to participate
 * in the online campaign run trough the website http://listerine.co.
 *
 * The application has several steps that expose the consumers to the good
 * practices of health care, assesses their oral health care through simple
 * questions and exposes them to the actual campaign and its benefits.
 *
 * ========================================================================================
 * @author Andrés Osorio, Chief developer at OWAK Digital Agency <AndresOsorio@owak.co>
 * @date 2015-03-20
 * @version 1.0.0
 * ========================================================================================
 */

/**
 * Main application module
 *
 * this module requires all the necessary dependencies for the application to work,
 * by importing all the controllers, filters, directives and services built
 * for this application
 *
 * @type {*|module}
 */
var Application = angular.module('ListerineReto21Dias', [
    'ionic',
    'ngCordova',
    'cgBusy',
    'ct.ui.router.extras',
    'ListerineReto21Dias.controllers',
    'ListerineReto21Dias.services',
    'ListerineReto21Dias.directives'
]);

/**
 * Configure Ionic Framework specific values
 */
Application.run(function ( $ionicPlatform ) {
    $ionicPlatform.ready(function () {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if ( window.cordova && window.cordova.plugins.Keyboard ) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if ( window.StatusBar ) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
})

/**
 * Routing configuration
 *
 * Defines all possible states and "screens" the application has.
 * It uses UI-Router to define main states, substates, their specific controller
 * and the location of every template and partial that should be rendered in
 * every defined state3
 */
Application.config(function ( $stateProvider, $urlRouterProvider ) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider
        .state('intro', {
            url         : '/',
            templateUrl : "templates/intro.html",
            controller  : "ApplicationController"
        })
        // setup the login view state
        .state('login', {
            url         : "/login",
            templateUrl : "templates/user/login.html",
            controller  : 'LoginController'
        })

        // setup for the registration view
        .state('register', {
            url         : '/register',
            templateUrl : 'templates/user/register.html',
            controller  : 'RegisterController'
        })

        /*
         * Assessment views
         */
        //index view (contains all substates in its template)
        .state('assessment', {
            url         : '/assessment',
            templateUrl : 'templates/assessment/index.html',
            controller  : 'AssessmentController'
        })
        //assessment questions state
        .state('assessment.questions', {
            url         : '/questions',
            templateUrl : 'templates/assessment/questions.html'
        })
        //assessment results state
        .state('assessment.result', {
            url         : '/results',
            templateUrl : 'templates/assessment/results.html'
        })

        /*
         * Oral Care Steps views
         */
        //index view (layout)
        .state('oralcare-steps', {
            url         : '/oralcare/steps',
            templateUrl : 'templates/oral_care/steps/index.html',
            controller  : 'OralCareStepsController'
        })
        //oral care step: brushing
        .state('oralcare-steps.brushing', {
            url         : '/brushing',
            templateUrl : 'templates/oral_care/steps/brushing.html'
        })
        //oral care step: flossing
        .state('oralcare-steps.flossing', {
            url         : '/flossing',
            templateUrl : 'templates/oral_care/steps/flossing.html'
        })
        //oral care step: mouthwash
        .state('oralcare-steps.mouthwash', {
            url         : '/mouthwash',
            templateUrl : 'templates/oral_care/steps/mouthwash.html'
        })

        /*
         * Oral Care Tips Views
         */
        //index view (layout)
        .state('oralcare-tips', {
            url         : '/oralcare/tips',
            templateUrl : 'templates/oral_care/tips/index.html',
            controller  : 'OralCareTipsController'
        })
        //oral care tips for teeth brushing
        .state('oralcare-tips.brushing', {
            url         : '/brushing',
            templateUrl : 'templates/oral_care/tips/brushing.html'
        })
        //oral care tips for flossing
        .state('oralcare-tips.flossing', {
            url         : '/flossing',
            templateUrl : 'templates/oral_care/tips/flossing.html'
        })
        //oral care tips for mouthwshing
        .state('oralcare-tips.mouthwash', {
            url         : '/mouthwash',
            templateUrl : 'templates/oral_care/tips/mouthwash.html'
        })

        /*
         * Exposing the Challenge views
         */
        //introduction screen
        .state('challenge-intro', {
            url         : '/challenge-intro',
            templateUrl : 'templates/challenge/intro.html'
        })
        //challenge index view (layout)
        .state('challenge', {
            url         : '/challenge',
            templateUrl : 'templates/challenge/index.html',
            controller  : 'ChallengeController'
        })
        //challenge info for day 1
        .state('challenge.day1', {
            url         : '/day-1',
            templateUrl : 'templates/challenge/challenge.day1.html'
        })
        //challenge info for day 7
        .state('challenge.day7', {
            url         : '/day-7',
            templateUrl : 'templates/challenge/challenge.day7.html'
        })
        //challenge info for day 14
        .state('challenge.day14', {
            url         : '/day-14',
            templateUrl : 'templates/challenge/challenge.day14.html'
        })
        //challenge info for day 21
        .state('challenge.day21', {
            url         : '/day-21',
            templateUrl : 'templates/challenge/challenge.day21.html'
        })

        /*
         * Engage user into the challenge (email form)
         */
        .state('user-engage', {
            url         : '/user-engage',
            templateUrl : 'templates/user/engage.html',
            controller  : 'UserEngageController'
        })

        //product placement screen
        .state('listerine', {
            url         : '/listerine-family',
            templateUrl : 'templates/listerine.html'
        })

        /*
         * additional states that do not have any interaction
         */
        //static screen for describing the challenge
        .state('about-the-challenge', {
            url         : '/about-the-challenge',
            templateUrl : 'templates/about.html'
        })

        /*
         * legal views
         */
        //index view (layout)
        .state('legals', {
            url         : '/legals',
            templateUrl : 'templates/legals/index.html'
        })
        //privacy policy
        .state('legals.privacy-policy', {
            url         : '/privacy-policy',
            templateUrl : 'templates/legals/privacy-policy.html'
        })
        //terms and conditions
        .state('legals.terms-and-conditions', {
            url         : '/terms-and-conditions',
            templateUrl : 'templates/legals/terms-and-conditions.html'
        })
        //legal notice
        .state('legals.legal-note', {
            url         : '/legal-note',
            templateUrl : 'templates/legals/legal-note.html'
        })

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/');
});

/**
 * basic configuration of cgBusy library (for loading states)
 */
Application.value('cgBusyDefaults',{
    message     :'Por favor, espera...',
    backdrop    : false,
    templateUrl : 'templates/loading.html',
    delay       : 300,
    minDuration : 700,
    wrapperClass: 'loading'
});

/**
 * Configure HttpProvider interceptors for authentication
 */
Application.config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    //delete $httpProvider.defaults.headers.common['X-Requested-With'];
    $httpProvider.interceptors.push('sessionInjector');
}]);