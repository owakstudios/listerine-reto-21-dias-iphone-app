# LISTERINE® Reto 21 Dias - HCP Mobile App for iOS


## Overview

This is a custom hybrid iPhone application built for oral care professionals who recommend the use of LISTERINE® to their patients. The purpose is to involve the HCP professionals into the campaign "Reto 21 dias" by becoming "influencers" or "evangelists" that drive their patients to participate in the online campaign run trough the website [http://listerine.co.](http://listerine.co)

The application has several steps that expose the consumers to the good practices of health care, assesses their oral health care through simple questions and exposes them to the actual campaign and its benefits.

---

## Installation

Clone this repository into any directory in your system.

	git clone https://[your user-here]@bitbucket.org/owakstudios/listerine-reto-21-dias-backend-server.git

You must have the latest version of [nodeJS](https://nodejs.org/) intalled. This app uses the [Ionic Framework](http://ionicframework.com) for bootstrapping and application compiling and management. Make sure you have ionic installed by typing in your terminal or command line:

	ionic -v
	
if not, install ionic and all its dependencies by typing:

	sudo npm install -g cordova ionic
	
Next, install all necessary node depdencies for running and testing the application in your system. In your terminal, `cd` into the directory where you cloned the repository and type:

	sudo npm install

Then, install al vendor and library dependencies that the app need in order to run (see the depdencies section below) by running the command:

	bower install
	
and you're ready to go!

---

## Development and Testing

You can run a simple server to test the change you make to the application by running:

	ionic serve
	
The server has livereload configured, so your changes will instantly load once you have made them!

If you want to see a simulation of the app in an iPhone or android viewport, run

	ionic serve --lab
	
to get a testing environment similar to the image below:
![ionic lab](http://ionicframework.com/img/blog/lab.png)

visit the [ionic documentation](http://ionicframework.com/docs/cli/test.html) for additional information.

---

## Compiling and running

all the commands listed below accept [additional configuration options](http://ionicframework.com/docs/cli/run.html) you can find in the [ionic framework documentation](http://ionicframework.com/docs/).

#### exporting the app to a platform
In order to export the native application to the desired platform, you must first add the desired platform to the project. run the command

	ionic platform add ios

to generate all the files the iOS platform files necessary for exporting the native app (this generates an xcode project with all the compiled CSS, JS, etc of the application). The files will be created in the `/platforms/<platform_name>` directory.

#### rebuilding the application for a platform

To re-build the application once you have done any change, run

	ionic build <platform_name>

To rebuild and re-generate all the files with the updates you've made to the native files for the platform

#### emulating the app in platform simulators

If you don´t want to open the xcode project or android studio project to test your app in the simulator, simply run the command:

	ionic emulate ios
	
to build deploy the application into the desired simulator

---

## Testing in devices

To test the application in the actual devices, you must have a developer account for either apple or android to deploy the application to testing devices.

Ionic has a utility application called [ionic view](http://view.ionic.io) to test your application in the device without the need of developer accounts. [Sign up](https://apps.ionic.com/signup) into ionic view website. Once you have your account. `cd` into your project and type

	ionic login
	ionic upload
	
Once the upload finishes, you can access a preview of the app in the ionic view application.

---

## Dependencies

* AngularJS
* Angular ui-router (included in ionic)
* jQuery 1.10
* snap.svg (for manipulating SVG)
* angular busy (for activity indicators)

Take a look at `bower.json` file for additional dependencies.

### adding dependencies

You can add your own dependencies to the application by typing the command

	bower install <your_dependency_name> --save
	
to install the dependency and save it to the `bower.json` file.

---

## A Note on SASS

Ionic uses SASS to compile all its styles that give form to the framework. Ionic is placed as an additional dependency in the `www/lib` directory. If you want to edit the default styles of ionic to fit your needs, **you must add it to version control** or all changes will be lost the next time you build the application.

---

## More Info

Powered by OWAK Digital Agency.
Author: Andrés Osorio <AndresOsorio@owak.co>